﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AW_WCF
{
    public static  class DatabaseManager
    {
        public static void SaveShiftToDb(ShiftModel shift)
        {
            var shiftEntity = new ShiftsSet
            {
                Name = shift.ShiftName,
                Date = shift.Date
            };

            var db = new OskarDbEntities();
            db.ShiftsSet.Add(shiftEntity);

            db.SaveChanges();
        }

        internal static bool IsShiftInDb(ShiftModel shift)
        {
            var db = new OskarDbEntities();
            foreach (var shiftInDb in db.ShiftsSet)
            {
                if (shiftInDb.Date.Equals(shift.Date) &&
                    shiftInDb.Name.Equals(shift.ShiftName))
                {
                    return true;
                }
            }

            return false;
        }
    }
}