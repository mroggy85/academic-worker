﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Web;
using System.Xml;
using System.Xml.Linq;

namespace AW_WCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    [ServiceBehavior(AddressFilterMode = AddressFilterMode.Any)]
    public class AWService : IAWService
    {
        public string Search()
        {
            const string LOGIN_URL = "http://www.academicwork.se/default.aspx";
            const string USERNAME = "soma.mattsson@gmail.com";
            const string PASSWORD = "handboll";

            // Request the login form to get the viewstate value
            HttpWebRequest webRequest = WebRequest.Create(LOGIN_URL) as HttpWebRequest;
            string response1 = new StreamReader(webRequest.GetResponse().GetResponseStream()).ReadToEnd();

            // Extract the viewstate value and build our POST data
            string viewState = ExtractViewState(response1);
            string eventValidation = ExtractEventValidation(response1);
            string postData = String.Format(
                     "__VIEWSTATE={0}&ctl00$LoginForm1$TextBoxUserName={1}&ctl00$LoginForm1$TextBoxPassword={2}&__EVENTTARGET=ctl00$LoginForm1$ButtonLogin&__EVENTVALIDATION={3}",
                     viewState, USERNAME, PASSWORD, eventValidation);

            // Set up the Request properties
            webRequest = WebRequest.Create(LOGIN_URL) as HttpWebRequest;
            webRequest.Method = "POST";
            webRequest.ContentType = "application/x-www-form-urlencoded";
            CookieContainer cookies = new CookieContainer();
            webRequest.CookieContainer = cookies;

            // Post back to the form
            using (StreamWriter requestWriter = new StreamWriter(webRequest.GetRequestStream()))
            {
                requestWriter.Write(postData);
            }

            // Read response
            string response2 = new StreamReader(webRequest.GetResponse().GetResponseStream()).ReadToEnd();

            
            // Set up the Request properties
            webRequest = WebRequest.Create("http://www.academicwork.se/candidates/workplan.aspx") as HttpWebRequest;
            webRequest.Method = "GET";
            webRequest.CookieContainer = cookies;

            // Read response
            string response3 = new StreamReader(webRequest.GetResponse().GetResponseStream()).ReadToEnd();
            

            // Set up the Request properties
            webRequest = WebRequest.Create("http://workplan.academicwork.se/Pages/lediga-pass.aspx") as HttpWebRequest;
            webRequest.Method = "GET";
            webRequest.CookieContainer = cookies;

            // Read response
            string response4 = new StreamReader(webRequest.GetResponse().GetResponseStream()).ReadToEnd();
            
            var table = ExtractTable(response4);

            var xmlDoc = FromTableToXml(table);

            var list = CreateListOfShifts(xmlDoc);

            list = FilterListForNewShifts(list);

            var resultString = "";
            if (list.Count == 0)
                resultString = "Inga nya pass.";
            else
            {
                resultString = "\nNya pass:\n";
                resultString += "----------------------------------\n\n";
                foreach (var shift in list)
                {
                    resultString += "Datum: " + shift.Date + " Dag: " + shift.WeekDay + " Avdelning: " + shift.ShiftName + " Platser kvar: " + shift.Bookings;
                    resultString += "\n";
                }
                resultString += "----------------------------------";
            }

            return resultString;
        }

        private List<ShiftModel> FilterListForNewShifts(List<ShiftModel> list)
        {
            var filteredList = new List<ShiftModel>();
            foreach (var shift in list)
            {
                if (!DatabaseManager.IsShiftInDb(shift))
                {
                    filteredList.Add(shift);
                    DatabaseManager.SaveShiftToDb(shift);
                } 
            }

            return filteredList;
        }

        private XmlDocument FromTableToXml(string table)
        {
            int counter = 0;
            var xml = "";
            while (table.Length > 0 && counter < 500)
            {
                var startIndex = table.IndexOf("<");
                var endIndex = table.IndexOf(">");
                var xmlToAdd = table.Substring(startIndex, endIndex - startIndex + 1);
                if (xmlToAdd.Contains("href"))
                {
                    xmlToAdd = xmlToAdd.Remove(2, xmlToAdd.Length - 3);
                }

                xml += xmlToAdd;
                table = table.Remove(startIndex, endIndex - startIndex + 1);

                if (!xmlToAdd.Contains("/"))
                {
                    endIndex = table.IndexOf("<");
                    xmlToAdd = table.Substring(0, endIndex);
                    xml += xmlToAdd;
                    table = table.Remove(0, endIndex);
                }
                counter++;
            }
            xml = xml.Replace("&", "");

            var xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(xml);

            return xmlDoc;
        }

        private List<ShiftModel> CreateListOfShifts(XmlDocument xmlDoc)
        {
            var listOfShifts = new List<ShiftModel>();

            var tableNode = xmlDoc.ChildNodes[0];
            for (int i = 1; i < tableNode.ChildNodes.Count; i++)
            {
                

                var td = tableNode.ChildNodes[i];

                var shift = new ShiftModel
                {
                    Date = td.ChildNodes[1].ChildNodes[0].Value,
                    WeekDay = td.ChildNodes[2].ChildNodes[0].Value,
                    ShiftName = td.ChildNodes[3].ChildNodes[0].ChildNodes[0].Value,
                    Mission = td.ChildNodes[4].ChildNodes[0].Value,
                    Client = td.ChildNodes[5].ChildNodes[0].Value,
                    Bookings = td.ChildNodes[6].ChildNodes[0].Value
                };
                shift.ShiftName = shift.ShiftName.Replace("nbsp;", " ");
                listOfShifts.Add(shift);

                //for (int j = 0; j < td.ChildNodes.Count; j++)
                //{
                //    var node = td.ChildNodes[j].ChildNodes[0].Value;
                //    //node.ToString();
                //}
            }

            return listOfShifts;
        }

        private string ExtractTable(string s)
        {
            var tableStart = "<table class=\"tableContent\" cellspacing=\"0\" rules=\"all\" id=\"ctl00_cphMain_DG_FreeShiftList\" width=\"100%\">";
            var tableStartIndex = s.IndexOf(tableStart);
            
            var table = s.Substring(tableStartIndex);
            var tableEndIndex = table.IndexOf("</table>");
            table = table.Remove(tableEndIndex + 8);

            table = table.Replace("\n", String.Empty);
            table = table.Replace("\r", String.Empty);
            table = table.Replace("\t", String.Empty);
            
            return table;
        }

        private string ExtractViewState(string s)
        {
            string viewStateNameDelimiter = "__VIEWSTATE";
            string valueDelimiter = "value=\"";

            int viewStateNamePosition = s.IndexOf(viewStateNameDelimiter);
            int viewStateValuePosition = s.IndexOf(
                  valueDelimiter, viewStateNamePosition
               );

            int viewStateStartPosition = viewStateValuePosition +
                                         valueDelimiter.Length;
            int viewStateEndPosition = s.IndexOf("\"", viewStateStartPosition);

            return HttpUtility.UrlEncodeUnicode(
                     s.Substring(
                        viewStateStartPosition,
                        viewStateEndPosition - viewStateStartPosition
                     )
                  );
        }

        private string ExtractEventValidation(string s)
        {
            string viewStateNameDelimiter = "__EVENTVALIDATION";
            string valueDelimiter = "value=\"";

            int viewStateNamePosition = s.IndexOf(viewStateNameDelimiter);
            int viewStateValuePosition = s.IndexOf(
                  valueDelimiter, viewStateNamePosition
               );

            int viewStateStartPosition = viewStateValuePosition +
                                         valueDelimiter.Length;
            int viewStateEndPosition = s.IndexOf("\"", viewStateStartPosition);

            return HttpUtility.UrlEncodeUnicode(
                     s.Substring(
                        viewStateStartPosition,
                        viewStateEndPosition - viewStateStartPosition
                     )
                  );
        }

        public string Test()
        {
            return "Test slutfördes!";
        }

    }
}
