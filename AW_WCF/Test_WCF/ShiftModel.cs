﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AW_WCF
{
    public class ShiftModel
    {
        public string Date { get; set; }
        public string WeekDay { get; set; }
        public string ShiftName { get; set; }
        public string Mission { get; set; }
        public string Client { get; set; }
        public string Bookings { get; set; }
    }
}