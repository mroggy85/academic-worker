﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    public static class MailManager
    {
        private static SmtpClient smtp;
        private static MailMessage message;

        public static void SendMail(string body)
        {
            CreateMessage();
            message.Body = body;

            CreateSmtpClient();
            smtp.Send(message);
        }

        private static void CreateMessage()
        {
            message = new MailMessage();
            message.To.Add("soma.mattsson@gmail.com");
            message.Subject = "AW Rapport: Ledigt pass!!!";
            message.From = new MailAddress("lindgren.oskar@gmail.com");
            message.CC.Add("lindgren.oskar@gmail.com");
            message.Priority = MailPriority.High;
        }

        private static void CreateSmtpClient()
        {
            smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential("lindgren.oskar", "Svullo85a")
            };
        }
    }
}
