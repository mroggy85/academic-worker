﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows.Forms;
using mshtml;

namespace WindowsFormsApplication1
{

    [ComVisible(true)]
    public partial class Form1 : Form
    {
        private int stepsInWorkPlan;
        private const int noOfWeeksToSearch = 2;
        private int weeksSearched;

        private TimeSpan maxAppExecutionTime;
        private TimeSpan appExecutuionTime;
        private System.Timers.Timer timer;
        
        private bool appFinished;
        private const int ticksUntilExitTime = 5;
        private int ticksAfterAppFinished;

        private PassManager shiftObject;

        public Form1()
        {
            InitializeComponent();

            appExecutuionTime = new TimeSpan();
            maxAppExecutionTime = new TimeSpan(0, 1, 0);
            
            appFinished = false;
            ticksAfterAppFinished = 0;

            timer = new System.Timers.Timer(1000);
            timer.Elapsed += timer_Elapsed;
            timer.Start();
            

            this.ShowInTaskbar = false;
            this.WindowState = FormWindowState.Minimized;
            
            notifyIcon1.ShowBalloonTip(60 * 1000, "Academic Worker", "Kör...", ToolTipIcon.Info);
            //notifyIcon1.BalloonTipClosed += notifyIcon1_BalloonTipClosed;

            webBrowser1.Navigate("http://www.academicwork.se/careerbook/login.aspx");
            webBrowser1.DocumentCompleted += webBrowser1_DocumentCompleted;
            webBrowser1.ObjectForScripting = this;

            progressBar1.Maximum = 6;
            progressBar1.Value = 0;

            weeksSearched = 0;
            shiftObject = new PassManager
            {
                kran = new kran { ledigaPass = new ledigaPass[0] },
                bredgång = new Bredgång { ledigaPass = new ledigaPass[0] }
            };
        }

        void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            appExecutuionTime = appExecutuionTime.Add(new TimeSpan(0, 0, 1));
            if (appExecutuionTime > maxAppExecutionTime)
            {
                PrintErrorToLog();
                Application.Exit();
            }
            else if (appFinished)
            {
                ticksAfterAppFinished++;
                if (ticksAfterAppFinished > ticksUntilExitTime)
                    Application.Exit();
            }
        }

        void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            // 1. GoTo Login-page
            if (e.Url.AbsoluteUri == "http://www.academicwork.se/careerbook/login.aspx")
            {
                Login();
            }
            // 2. GoTo 'Schemaläggning'
            else if (e.Url.AbsoluteUri == "http://www.academicwork.se/careerbook/")
            {
                GoToSchemaLaggning();
            }
            // 3. GoTo 'Boka Arbetspass'
            else if (e.Url.AbsoluteUri == "http://workplan.academicwork.se/Pages/start.aspx" && stepsInWorkPlan == 0)
            {
                GoToBokaArbetspass();
                stepsInWorkPlan++;
            }
            // 4. Search for 'kranpass'
            else if (e.Url.AbsoluteUri == "http://workplan.academicwork.se/Pages/start.aspx" && stepsInWorkPlan >= 1)
            {
                SearchForKranpass();
                stepsInWorkPlan++;
            }
            // 5... Search for 'kranpass'
            else if (e.Url.AbsoluteUri == "http://workplan.academicwork.se/Pages/schema.aspx" && stepsInWorkPlan >= 1)
            {
                SearchForKranpass();
                stepsInWorkPlan++;
            }
        }

        private void Login()
        {
            AppendScript(new Func<string>(LoginScript), "awLogin");
            progressBar1.Value++;
            listBox1.Items.Clear();
            listBox1.Items.Add("Skriver in uppgifter och loggar in");
        }

        private void GoToSchemaLaggning()
        {
            listBox1.Items.Clear();
            listBox1.Items.Add("Navigerar till 'Schemaläggning'");

            AppendScript(new Func<string>(GoToSchemaLaggningScript), "awGoToSchemaLaggning");

            progressBar1.Value++;
        }

        private void GoToBokaArbetspass()
        {
            listBox1.Items.Clear();
            listBox1.Items.Add("Navigerar till 'Boka Arbetspass'");

            AppendScript(new Func<string>(GoToBokaArbetspassScript), "awGoToBokaArbetspass");

            progressBar1.Value++;
        }

        private void GoToNextWeek()
        {
            listBox1.Items.Clear();
            listBox1.Items.Add("Navigerar till nästa vecka");

            AppendScript(new Func<string>(GoToNextWeekScript), "awGoToNextWeek");

            progressBar1.Value++;
        }

        private void SearchForKranpass()
        {
            listBox1.Items.Clear();
            listBox1.Items.Add("Söker efter lediga pass");

            // Register passManager-object
            webBrowser1.Document.InvokeScript("eval", new object[] { RegisterPassManagerScript() });
            // Search for shifts and add result to passManager
            AppendScript(new Func<string>(SearchForKranpassScript), "awSearchForKranPass");

            progressBar1.Value++;
        }

        public void SearchFinished(string passManager)
        {
            passManager = passManager.Replace("\"", "");
            var serializer = new JavaScriptSerializer();
            var json = serializer.Deserialize<PassManager>(passManager);

            shiftObject.kran.antal += json.kran.antal;
            shiftObject.kran.ledigaPass.Concat(json.kran.ledigaPass);
            shiftObject.bredgång.antal += json.bredgång.antal;
            shiftObject.bredgång.ledigaPass.Concat(json.bredgång.ledigaPass);

            var jsonMessageKran = "Hittade " + json.kran.antal + " kran-pass. Varav " + json.kran.ledigaPass.Length + " var lediga.";
            var jsonMessageBredgång = "Hittade " + json.bredgång.antal + " bredgång-pass. Varav " + json.bredgång.ledigaPass.Length + " var lediga.";

            listBox1.Items.Clear();
            listBox1.Items.Add("Klart.");
            listBox1.Items.Add(jsonMessageKran);
            listBox1.Items.Add(jsonMessageBredgång);

            
            if (shiftObject.kran.ledigaPass.Length > 0)
            {
                var message = "Hej Sofia!\n\n" +
                              "Det finns " + shiftObject.kran.ledigaPass.Length + " lediga kran-pass!\n\n";

                foreach (var pass in shiftObject.kran.ledigaPass)
                {
                    message += "> " + pass.datum + " - " + pass.tid + "\n";
                }

                MailManager.SendMail(message);
            }

            weeksSearched++;
            if (weeksSearched < noOfWeeksToSearch)
            {
                GoToNextWeek();
            }
            else 
            {
                PrintToLog();
                PrintBaloonMessage();
                appFinished = true;
            }

        }

        //private int ballonTipCount = 0;
        //void notifyIcon1_BalloonTipClosed(object sender, EventArgs e)
        //{
        //    if (ballonTipCount > 0)
        //        Application.Exit();

        //    ballonTipCount++;
        //}

        private void PrintBaloonMessage()
        {
            if (shiftObject.kran.ledigaPass.Length > 0)
            {
                notifyIcon1.ShowBalloonTip(10000, "Academic Worker", "Hittade kran-pass!!!", ToolTipIcon.Warning);
            }
            else
            {
                notifyIcon1.ShowBalloonTip(2000, "Academic Worker", "Hittade inga pass", ToolTipIcon.Info);
            }
            
        }

        private void PrintToLog()
        {
            var time = DateTime.Now.ToString();
            var kran = shiftObject.kran;
            var bred = shiftObject.bredgång;
            var message = time + "| KRAN { antal: "+ kran.antal + " lediga: " + kran.ledigaPass.Length + "} " + 
                                " | BREDGÅNG { antal: "+ bred.antal +", lediga: "+ bred.ledigaPass.Length +"}";

            writeToFile("Log.txt", message);
        }

        private void PrintErrorToLog()
        {
            var time = DateTime.Now.ToString();
            var message = time + "| ERROR: Timeout { app took over " + maxAppExecutionTime.Minutes + "min to complete task. }";

            writeToFile("Log.txt", message);
        }

        public class PassManager
        {
            public kran kran { get; set; }
            public Bredgång bredgång { get; set; }
        }

        public class kran
        {
            public int antal { get; set; }
            public ledigaPass[] ledigaPass { get; set; }
        }

        public class Bredgång
        {
            public int antal { get; set; }
            public ledigaPass[] ledigaPass { get; set; }
        }

        public class ledigaPass
        {
            public string datum { get; set; }
            public string tid { get; set; }
        }

        private string RegisterPassManagerScript()
        {
            return readFromFile("RegisterPassManager.js");
        }

        private string LoginScript()
        {
            return "function awLogin() { " +
                "document.getElementById('TextBoxUserName').value = 'soma.mattsson@gmail.com';" +
                "document.getElementById('TextBoxPassword').value = 'handboll';" +
                "document.getElementById('ButtonLogin').click();}";
        }

        private string GoToSchemaLaggningScript()
        {
            return "function awGoToSchemaLaggning() { " +
                "var sidebar = document.getElementById('sidebar-left');" +
                "sidebar.children[0].children[0].children[1].children[4].children[0].target = '';" +
                "sidebar.children[0].children[0].children[1].children[4].children[0].click();}";
        }

        private string GoToBokaArbetspassScript()
        {
            return "function awGoToBokaArbetspass() { " +
                "document.getElementById('ctl00_cphMainLeft_BTN_DoBookings').click();}";
        }

        private string GoToNextWeekScript()
        {
            return "function awGoToNextWeek() {" +
                "document.getElementById('ctl00_cphInPageMenu_LB_NextWeekStep').click();}";
        }

        private string LoadJQueryScript()
        {
            return "function awLoadJQuery() { " +
                "var head = document.getElementsByTagName('head')[0];" +
                "jquery = document.createElement('script');" +
                "jquery.src = 'http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.1.min.js';" +
                "jquery.type = 'text/javascript';" +
                //"jquery.onreadystatechange = function () {" +
                //"   if (this.readyState == 'complete') {" +
                //"       awSearchForKranpass();" +
                //"   }" +
                //"};" +
                "head.appendChild(jquery);}";
        }

        private string SearchForKranpassScript()
        {
            return readFromFile("awSearchForKranPass.js");       
        }

        private string testScript()
        {
            return "alert('testing!')";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Login();
        }

        private void AppendScript(Delegate method, string function)
        {
            var script = webBrowser1.Document.CreateElement("script");
            var element = (IHTMLScriptElement)script.DomElement;
            element.text = (string)method.DynamicInvoke();

            var head = webBrowser1.Document.GetElementsByTagName("head")[0];
            head.AppendChild(script);

            webBrowser1.Document.InvokeScript(function);
        }

        private void AppendScriptAttachEvent(string function, EventHandler method)
        {
            var script = webBrowser1.Document.CreateElement("script");
            var element = (IHTMLScriptElement)script.DomElement;
            element.text = function;

            var head = webBrowser1.Document.GetElementsByTagName("head")[0];
            head.AppendChild(script);
            script.AttachEventHandler("onreadystatechange", method);
        }

        private string readFromFile(string javascriptFile)
        {
            var reader = new StreamReader(new FileStream(javascriptFile, FileMode.Open));
            var content = reader.ReadToEnd();
            content = content.Replace("\r\n", "");

            reader.Close();

            return content;
        }

        private void writeToFile(string fileName, string message)
        {
            var writer = new StreamWriter(new FileStream(fileName, FileMode.Append));
            writer.WriteLine(message);

            writer.Close();

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            webBrowser1.ObjectForScripting = this;
            
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.ShowInTaskbar = true;
            this.WindowState = FormWindowState.Normal;
        }
    }
}
