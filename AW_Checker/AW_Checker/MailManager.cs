﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace AW_Checker
{
    public static class MailManager
    {
        private static SmtpClient smtp;
        private static MailMessage message;

        public static void SendCriticalMail()
        {
            CreateMessage(MailPriority.High, "AW is Down!!!");
            message.Body = "Academic Worker är nere!\n\n" + 
                           "Åtgärda snarast!";

            CreateSmtpClient();
            smtp.Send(message);
        }

        public static void SendNormalMail()
        {
            CreateMessage(MailPriority.Low, "Normalt");
            message.Body = "Academic Worker körs som vanligt.";

            CreateSmtpClient();
            smtp.Send(message);
        }

        private static void CreateMessage(MailPriority priority, string subject)
        {
            message = new MailMessage();
            //message.To.Add("soma.mattsson@gmail.com");
            message.To.Add("lindgren.oskar@hotmail.com");
            message.Subject = "AW Checker: " + subject;
            message.From = new MailAddress("lindgren.oskar@gmail.com");
            //message.CC.Add("lindgren.oskar@gmail.com");
            message.Priority = priority;
        }

        private static void CreateSmtpClient()
        {
            smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential("lindgren.oskar", "Svullo85a")
            };
        }
    }
}
