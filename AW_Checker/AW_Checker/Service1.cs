﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace AW_Checker
{
    public partial class Service1 : ServiceBase
    {
        private Timer _timer;
        private const int MinutesBetweenChecks = 60;

        public Service1()
        {
            InitializeComponent();
            _timer = new Timer();
            //_timer.Interval = (MinutesBetweenChecks * 60) * 1000;
            _timer.Interval = 10000;
            _timer.Elapsed += _timer_Elapsed;
        }

        void _timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            _timer.Interval = (MinutesBetweenChecks * 60) * 1000;
            var shiftsAdatper = new ShiftDbDataSetTableAdapters.ShiftsLogTableAdapter();
            var shiftLog = shiftsAdatper.GetLatestDate();

            var lastShiftDate = shiftLog[0].Date;
            var threeHoursAgo = DateTime.Now.Subtract(new TimeSpan(3, 0, 0));

            if (lastShiftDate < threeHoursAgo)
            {
                MailManager.SendCriticalMail();
            }
            else
	        {
                MailManager.SendNormalMail();
	        }
        }

        protected override void OnStart(string[] args)
        {
            _timer.Start();

        }

        protected override void OnStop()
        {
            _timer.Stop();
        }
    }
}
