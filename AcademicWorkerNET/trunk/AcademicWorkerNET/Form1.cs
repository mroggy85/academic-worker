﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows.Forms;
using AcademicWorkerNET.DataObjects;
using mshtml;

namespace AcademicWorkerNET
{

    [ComVisible(true)]
    public partial class Form1 : Form
    {
        private System.Timers.Timer _timerSearch;
        private System.Timers.Timer _timerCountdown;
        private TimeSpan _timeUntilNextSearch;
        private int _millisecondsBetweenSearches;

        private SearchManager _searchMgr;

        delegate void SetTextCallback(string text);
        delegate void SetProgressCallback(int seconds);

        public Form1()
        {
            InitializeComponent();

            

            //appExecutuionTime = new TimeSpan();
            //maxAppExecutionTime = new TimeSpan(0, 1, 0);
            
            //appFinished = false;
            //ticksAfterAppFinished = 0;

            _timerSearch = new System.Timers.Timer();
            _timerSearch.Elapsed += timerSearch_Elapsed;

            _timerCountdown = new System.Timers.Timer();
            _timerCountdown.Interval = 1000;
            _timerCountdown.Elapsed += timerCountdown_Elapsed;
            //_timer.Start();

            _timeUntilNextSearch = new TimeSpan();
            //this.ShowInTaskbar = false;
            //this.WindowState = FormWindowState.Minimized;
            
            //notifyIcon1.ShowBalloonTip(60 * 1000, "Academic Worker", "Kör...", ToolTipIcon.Info);
            //notifyIcon1.BalloonTipClosed += notifyIcon1_BalloonTipClosed;

            //webBrowser.Navigate("http://www.academicwork.se/careerbook/login.aspx");
            webBrowser.DocumentCompleted += webBrowser1_DocumentCompleted;
            webBrowser.ObjectForScripting = this;
            

            progressBar1.Maximum = 4;
            progressBar1.Value = 0;

            //weeksSearched = 0;
            //shiftObject = new PassManager
            //{
            //    kran = new kran { ledigaPass = new ledigaPass[0] },
            //    bredgång = new Bredgång { ledigaPass = new ledigaPass[0] }
            //};

            _searchMgr = new SearchManager(webBrowser);
        }

        private void timerSearch_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            _timerSearch.Interval = _millisecondsBetweenSearches;

            webBrowser.Navigate("http://www.academicwork.se/careerbook/login.aspx");

            //_timerSearch.Stop();
            
            int minutes = 0;
            var ok = Int32.TryParse(txtTime.Text, out minutes);
            if (ok)
            {
                _timeUntilNextSearch = new TimeSpan(0, minutes, 0);
            }
        }

        private void timerCountdown_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            var one_sec = new TimeSpan(0, 0, 1);
            _timeUntilNextSearch = _timeUntilNextSearch.Subtract(one_sec);
            

            var time = _timeUntilNextSearch.Minutes + ":" + _timeUntilNextSearch.Seconds;
            if (txtTimeLeft.InvokeRequired)
            {
                var d = new SetTextCallback(SetText);
                Invoke(d, time);
            }
            else
                this.txtTimeLeft.Text = time;

            if (progressTimeToNext.InvokeRequired)
            {
                var d = new SetProgressCallback(SetProgress);
                Invoke(d, (int) _timeUntilNextSearch.TotalSeconds);
            }
            else
                this.progressTimeToNext.Value = (int) _timeUntilNextSearch.TotalSeconds;

            //Action action = () => txtTimeLeft.Text = _timeUntilNextSearch.Minutes + ":" + _timeUntilNextSearch.Seconds;
            //Invoke(action);

            //setCountdown(_timeUntilNextSearch.Minutes + ":" + _timeUntilNextSearch.Seconds);
        }

        private void SetText(string text)
        {
            this.txtTimeLeft.Text = text;
        }

        private void SetProgress(int seconds)
        {
            this.progressTimeToNext.Value = seconds;
        }

        private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            var message = _searchMgr.ActionBasedOnUrl(e.Url.AbsoluteUri);

            if (message != null)
            {
                progressBar1.Value++;
                listBox1.Items.Clear();
                listBox1.Items.Add(message);
            }
        }

        public void SearchFinished(string passArray)
        {
            passArray = passArray.Replace("\\", "");
            var serializer = new JavaScriptSerializer();
            var shiftsFromSite = serializer.Deserialize<List<Shift>>(passArray);

            // Filter the shift-list through database
            var shiftToNotify = new List<Shift>();
            foreach (var shift in shiftsFromSite)
            {
                if (!DataLogic.existsInDb(shift.passtyp, shift.datum))
                {
                    shiftToNotify.Add(shift);
                    DataLogic.writeToDb(shift.passtyp, shift.datum);
                }
            }

            // Display all found shifts in app
            listBox1.Items.Clear();
            listBox1.Items.Add("Klart. " + DateTime.Now.ToShortDateString());
            if (shiftsFromSite.Count > 0)
            {
                foreach (var shift in shiftsFromSite)
                {
                    listBox1.Items.Add(shift.passtyp + " " + shift.datum + " " + shift.veckodag + " " + shift.bokningar);
                }
            }
            progressBar1.Value = 0;
           
            // Mail all new shifts
            if (shiftToNotify.Count > 0)
            {
                var messageToMail = "Hej Sofia!\n\n" +
                    "Det finns " + shiftToNotify.Count + " nya lediga pass!\n\n";

                foreach (var shift in shiftToNotify)
                {
                    messageToMail += shift.passtyp + " " + shift.datum + " - " + shift.bokningar + "\n";
                }

                MailManager.SendMail(messageToMail);
            }

            // Log the result
            DataLogic.writeToDbLog(shiftsFromSite.Count, shiftToNotify.Count);
        }

        private void start_Click(object sender, EventArgs e)
        {
            // Take time
            int minutes = 0;
            var ok = Int32.TryParse(txtTime.Text, out minutes);

            // start timer
            if (ok)
            {
                _timerSearch.Stop();

                txtTime.Enabled = false;
                progressBar1.Value = 0;

                var milliseconds = (minutes * 60) * 1000;
                //_timerSearch.Interval = milliseconds;
                _millisecondsBetweenSearches = milliseconds;
                _timerSearch.Interval = 1;
                _timerSearch.Start();

                _timeUntilNextSearch = new TimeSpan(0, minutes, 0);
                progressTimeToNext.Maximum = minutes * 60;
                progressTimeToNext.Value = minutes * 60;
                _timerCountdown.Start();

                
            }
            else
                MessageBox.Show("Please use a valid number!");
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            _timerSearch.Stop();
            _timerCountdown.Stop();

            txtTime.Enabled = true;

            webBrowser.Stop();
            progressBar1.Value = 0;
        }
    }
}
