﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcademicWorkerNET.DataObjects
{
    public class Shifts
    {
        public List<Shift> ShiftList { get; set; }
    }

    public class Shift
    {
        public string veckonummer { get; set; }
        public string datum { get; set; }
        public string veckodag { get; set; }
        public string passtyp { get; set; }
        public string bokningar { get; set; }
    }
}
