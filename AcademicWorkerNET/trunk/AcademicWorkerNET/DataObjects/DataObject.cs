﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcademicWorkerNET.DataObjects
{
    public class PassManager
    {
        public kran kran { get; set; }
        public Bredgång bredgång { get; set; }
    }

    public class kran
    {
        public int antal { get; set; }
        public ledigaPass[] ledigaPass { get; set; }
    }

    public class Bredgång
    {
        public int antal { get; set; }
        public ledigaPass[] ledigaPass { get; set; }
    }

    public class ledigaPass
    {
        public string datum { get; set; }
        public string tid { get; set; }
    }
}
