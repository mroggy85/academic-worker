﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using mshtml;

namespace AcademicWorkerNET
{
    public static class DataLogic
    {
        public static void AppendScript(WebBrowser webBrowser, Delegate method, string function)
        {
            var script = webBrowser.Document.CreateElement("script");
            var element = (IHTMLScriptElement)script.DomElement;
            element.text = (string)method.DynamicInvoke();

            var head = webBrowser.Document.GetElementsByTagName("head")[0];
            head.AppendChild(script);

            webBrowser.Document.InvokeScript(function);
        }

        public static void AppendScriptAttachEvent(WebBrowser webBrowser, string function, EventHandler method)
        {
            var script = webBrowser.Document.CreateElement("script");
            var element = (IHTMLScriptElement)script.DomElement;
            element.text = function;

            var head = webBrowser.Document.GetElementsByTagName("head")[0];
            head.AppendChild(script);
            script.AttachEventHandler("onreadystatechange", method);
        }

        public static string readFromFile(string javascriptFile)
        {
            var reader = new StreamReader(new FileStream(javascriptFile, FileMode.Open));
            var content = reader.ReadToEnd();
            content = content.Replace("\r\n", "");

            reader.Close();

            return content;
        }

        public static void writeToFile(string fileName, string message)
        {
            var writer = new StreamWriter(new FileStream(fileName, FileMode.Append));
            writer.WriteLine(message);

            writer.Close();

        }

        public static void writeToDb(string name, string date)
        {
            var dataset = new ShiftDbDataSet();
            dataset.Shifts.AddShiftsRow(name, date);

            var shiftsAdapter = new ShiftDbDataSetTableAdapters.ShiftsTableAdapter();
            shiftsAdapter.Update(dataset);
        }

        public static bool existsInDb(string name, string date)
        {
            var shiftsAdapter = new ShiftDbDataSetTableAdapters.ShiftsTableAdapter();
            var shifts = shiftsAdapter.GetData();

            foreach (var shift in shifts)
	        {
                if (shift.Name.Equals(name) && shift.Date.Equals(date))
                {
                    return true;
                }
	        }

            return false;
        }

        public static void writeToDbLog(int ShiftsFound, int NewShifts)
        {
            var dataset = new ShiftDbDataSet();
            dataset.ShiftsLog.AddShiftsLogRow(DateTime.Now, ShiftsFound, NewShifts);

            var adapter = new ShiftDbDataSetTableAdapters.ShiftsLogTableAdapter();
            adapter.Update(dataset);
        }
    }
}
