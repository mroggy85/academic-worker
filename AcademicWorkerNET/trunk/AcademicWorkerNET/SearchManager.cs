﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AcademicWorkerNET
{
    public class SearchManager
    {
        private WebBrowser _webBrowser;

        public SearchManager(WebBrowser webBrowser)
        {
            _webBrowser = webBrowser;
        }

        public string ActionBasedOnUrl(string url)
        {
            // 1. GoTo Login-page
            if (url == "http://www.academicwork.se/careerbook/login.aspx")
            {
                return Login();
            }
            // 2. GoTo 'Schemaläggning'
            else if (url == "http://www.academicwork.se/careerbook/")
            {
                return GoToSchemaLaggning();
            }
            // 3. GoTo 'Lediga Pass'
            else if (url == "http://workplan.academicwork.se/Pages/start.aspx")
            {
                return GoToLedigaPass();
                
            }
            // 4. Search for 'kranpass'
            else if (url == "http://workplan.academicwork.se/Pages/lediga-pass.aspx")
            {
                return SearchForKranpass();
            }

            return null;
        }

        private string Login()
        {
            DataLogic.AppendScript(_webBrowser, new Func<string>(LoginScript), "awLogin");

            return "Skriver in uppgifter och trycker på knappen 'loggar in'";
        }

        private string GoToSchemaLaggning()
        {
            //DataLogic.AppendScript(_webBrowser, new Func<string>(GoToSchemaLaggningScript), "awGoToSchemaLaggning");
            _webBrowser.Navigate("http://www.academicwork.se/candidates/workplan.aspx");

            return "Navigerar via URL till 'Schemaläggning'";
        }

        private string GoToLedigaPass()
        {
            _webBrowser.Navigate("http://workplan.academicwork.se/Pages/lediga-pass.aspx");

            return "Navigerar via URL till 'Lediga pass'";
        }

        private string SearchForKranpass()
        {
            // Register passManager-object
            //_webBrowser.Document.InvokeScript("eval", new object[] { RegisterPassManagerScript() });
            // Search for shifts and add result to passManager
            DataLogic.AppendScript(_webBrowser, new Func<string>(SearchForKranpassScript), "awSearchForKranPass");

            return "Söker efter lediga pass";
        }

        private string SearchForKranpassScript()
        {
            return DataLogic.readFromFile("search_new.js");
        }

        private string LoginScript()
        {
            return "function awLogin() { " +
                "document.getElementById('TextBoxUserName').value = 'soma.mattsson@gmail.com';" +
                "document.getElementById('TextBoxPassword').value = 'handboll';" +
                "document.getElementById('ButtonLogin').click();}";
        }

        //private string GoToSchemaLaggningScript()
        //{
        //    return "function awGoToSchemaLaggning() { " +
        //        "var sidebar = document.getElementById('sidebar-left');" +
        //        "sidebar.children[0].children[0].children[1].children[4].children[0].target = '';" +
        //        "sidebar.children[0].children[0].children[1].children[4].children[0].click();}";
        //}

        //private string GoToBokaArbetspassScript()
        //{
        //    return "function awGoToBokaArbetspass() { " +
        //        "document.getElementById('ctl00_cphMainLeft_BTN_DoBookings').click();}";
        //}

        //private string GoToNextWeekScript()
        //{
        //    return "function awGoToNextWeek() {" +
        //        "document.getElementById('ctl00_cphInPageMenu_LB_NextWeekStep').click();}";
        //}

        //private string RegisterPassManagerScript()
        //{
        //    return DataLogic.readFromFile("RegisterPassManager.js");
        //}
    }
}
