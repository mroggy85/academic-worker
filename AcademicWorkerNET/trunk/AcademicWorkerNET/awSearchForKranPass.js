﻿function awSearchForKranPass() {

    var noOfTbody = document.getElementsByTagName('tbody').length;
    var tbody = document.getElementsByTagName('tbody')[noOfTbody - 1];
    for (i = 1; i < tbody.children.length; i++) {
        var tr = tbody.children[i];
        for (j = 1; j < tr.children.length; j++) {
            var td = tr.children[j];
            if (td.children.length > 0) {
                var a = td.getElementsByTagName('a');
                for (k = 0; k < a.length; k++) {
                    var pass = a[k];
                    var passBeskrivning = pass.getElementsByTagName('b')[0].innerText;
                    var passTid = passBeskrivning.substring(0, 5);
                    var passNamn = passBeskrivning.substring(6);
                    if (passNamn.search(/kran/i) != -1) {
                        passManager.kran.antal++;
                        var passBild = pass.getElementsByTagName('img')[0];
                        if (passBild != null && passBild.length > 0) {
                            var passStatus = passBild.getAttribute('src');
                            if (passStatus.search(/fullbokad/i) == -1) {
                                var passDatum = tbody.children[0].children[j].innerText;
                                passManager.kran.pass.push({ datum: passDatum, tid: passTid });
                            }
                        }
                        
                    }
                    else if (passNamn.search(/bredgång/i) != -1) {
                        passManager.bredgång.antal++;
                        var passBild = pass.getElementsByTagName('img')[0];
                        if (passBild != null && passBild.length > 0) {
                            var passStatus = passBild.getAttribute('src');
                            if (passStatus.search(/fullbokad/i) == -1) {
                                var passDatum = tbody.children[0].children[j].innerText;
                                passManager.bredgång.pass.push({ datum: passDatum, tid: passTid });
                            }
                        }
                    }
                }
            }
        }
    }

    var head = document.getElementsByTagName('head')[0];
    var script = document.createElement('script');
    script.src = 'http://yandex.st/json2/2011-10-19/json2.min.js';
    script.type = 'text/javascript';
    script.onreadystatechange = function () {
        var json = JSON.stringify(passManager);
        window.external.SearchFinished(json);
    };
    head.appendChild(script);
    

   
}